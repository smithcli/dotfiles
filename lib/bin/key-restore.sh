#!/usr/bin/env bash

# Import keys from backup
BACKUP_DIR="$HOME/.backup"
GPG_BACKUP_DIR="$BACKUP_DIR/gpg"
SSH_BACKUP_DIR="$BACKUP_DIR/ssh"
BACKUP_NAME="*-key-backup.tar.xz"
EXPORT_NAME="*-key-export.tar.xz"
OVERWRITE=false
KEY_EXPORT=false
log_error() { echo -e "\e[31m$*\e[0m"; }
log_info() { echo -e "\e[32m$*\e[0m"; }

# Print help information
print_help() {
  echo "Restores GnuPG keys and SSH from a backup archive created by the "
  echo "key-backup script."
  echo "Usage: $0 [OPTIONS]"
  echo "Options:"
  echo "  -x, --export       Restores keys from an export archive."
  echo "  -o, --overwrite    Overwrite existing files without creating backups."
  echo "  -h, --help         Display this help message and exit."
}

# Parse command-line arguments
while [[ "$1" != "" ]]; do
  case $1 in
  -o | --overwrite)
    OVERWRITE=true
    ;;
  -x | --export)
    KEY_EXPORT=true
    ;;
  --help)
    print_help
    exit 0
    ;;
  *)
    echo "Unknown option: $1"
    print_help
    exit 1
    ;;
  esac
  shift
done

# Function to search for the latest backup file
_find_latest_backup_file() {
  # Find all files matching the pattern and sort them by date
  local pattern=$1
  latest_backup=$(find "$BACKUP_DIR" -type f -name $pattern | sort -t '-' -k 1,1r | head -n 1)
  if [[ -z "$latest_backup" ]]; then
    return 1 # No backup files found
  else
    echo "$latest_backup"
    return 0 # Backup file found
  fi
}

_extract_backup() {
  local archive=$1
  tar -xJf "$archive" -C "$BACKUP_DIR"
}

# Restore GPG
_restore_gpg_keys() {
  local gpg_bk="$GPG_BACKUP_DIR"
  local gpg_home="$HOME/.gnupg"
  if [[ ! -d "$GPG_BACKUP_DIR" ]]; then
    log_error "No GnuPG backup directory found."
    return 1
  fi

  log_info "Importing keys from $gpg_bk \n"
  # generates gpg folder and keyring if not already
  gpg --version
  mkdir -p $gpg_home
  chmod 700 $gpg_home

  if [[ -f "$gpg_bk/pub.asc" ]]; then
    gpg --import "$gpg_bk/pub.asc"
  fi

  if [[ -f "$gpg_bk/priv.asc" ]]; then
    gpg --import "$gpg_bk/priv.asc"
  fi

  if [[ -f "$gpg_bk/trust.txt" ]]; then
    gpg --import-ownertrust <"$gpg_bk/trust.txt"
  fi

  local cp_options="-v"
  [[ $OVERWRITE == false ]] && cp_options+=" --backup=numbered"

  if [[ -f "$gpg_bk/gpg.conf" ]]; then
    cp $cp_options "$gpg_bk/gpg.conf" "$gpg_home"
  fi

  if [[ -f "$gpg_bk/common.conf" ]]; then
    cp $cp_options "$gpg_bk/common.conf" "$gpg_home"
  fi

  if [[ -f "$gpg_bk/gpg-agent.conf" ]]; then
    cp $cp_options "$gpg_bk/gpg-agent.conf" "$gpg_home"
  fi

  if [[ -f "$gpg_bk/sshcontrol" ]]; then
    cp $cp_options "$gpg_bk/sshcontrol" "$gpg_home"
  fi

  if [[ -f "$gpg_bk/gitoken.gpg" ]]; then
    cp $cp_options --update "$gpg_bk/gitoken.gpg" "$gpg_home"
  fi

  if [[ -d "$gpg_bk/openpgp-revocs.d" ]]; then
    mkdir -vp "$gpg_home/openpgp-revocs.d"
    cp --update "$gpg_bk/openpgp-revocs.d/"* "$gpg_home/openpgp-revocs.d"
  fi

  log_info "GnuPG keys and configuration restored."
}

_restore_ssh_keys() {
  local ssh_home="$HOME/.ssh"
  if [[ ! -d "$SSH_BACKUP_DIR" ]]; then
    log_error "No SSH backup directory found."
  fi
  mkdir -p $ssh_home
  chmod 700 $ssh_home
  local cp_options="-v"
  [[ $OVERWRITE == false ]] && cp_options+=" --backup=numbered"
  cp $cp_options "$SSH_BACKUP_DIR/"* "$ssh_home"
  log_info "SSH keys and configuration restored."
}

_cleanup_files() {
  # write over sensitive
  shred -u \
    $GPG_BACKUP_DIR/pub.asc \
    $GPG_BACKUP_DIR/priv.asc \
    $GPG_BACKUP_DIR/trust.txt \
    $GPG_BACKUP_DIR/gpg.conf \
    $GPG_BACKUP_DIR/gitoken.gpg \
    $GPG_BACKUP_DIR/openpgp-revocs.d/* \
    $SSH_BACKUP_DIR/*
  # remove directories
  rm -r $GPG_BACKUP_DIR
  rm -r $SSH_BACKUP_DIR
}

restore_backup() {
  log_info "Attempting to restore from key backup"
  # Find backup archive
  if [[ ! -d "$BACKUP_DIR" ]]; then
    log_error "Could not find backup directory: $BACKUP_DIR"
    return 1
  fi
  chmod 700 $BACKUP_DIR

  local pattern="$BACKUP_NAME"
  [[ $KEY_EXPORT == true ]] && pattern="$EXPORT_NAME"
  latest_backup=$(_find_latest_backup_file $pattern)
  if [[ $? -eq 0 ]]; then
    log_info "Found latest backup: $latest_backup"
  else
    log_error "No backup archives found"
    return 1
  fi

  # Extract archive
  _extract_backup $latest_backup

  # Import Keys
  _restore_gpg_keys
  _restore_ssh_keys

  # Cleanup
  _cleanup_files
  log_info "Key backup restored from $latest_backup"
}

restore_backup
