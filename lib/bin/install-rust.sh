#!/usr/bin/env bash

#Script to install Rust programming language

if [[ -z "$RUST_HOME" ]]; then
  export RUST_HOME="$HOME/.local/lib/rust"
fi

if [[ -z "$RUSTUP_HOME" ]]; then
  export RUSTUP_HOME="$RUST_HOME/rustup"
fi

if [[ -z "$CARGO_HOME" ]]; then
  export CARGO_HOME="$RUST_HOME/cargo"
else
  no_modify_path="--no-modify-path"
fi

echo "Installing Rustup"
mkdir -p $RUST_HOME
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y $no_modify_path

echo "Rustup installed at $RUSTUP_HOME"
echo "Cargo installed at $CARGO_HOME"
