#!/bin/bash

AppName=${PWD##*/}-node

podman run -it \
  --rm \
  -p 8000:8000 \
  -v .:/opt/$AppName:z \
  -w /opt/$AppName \
  --name $AppName \
  node:lts bash
