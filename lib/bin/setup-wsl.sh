#!/bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
YNPROMPT="$SCRIPT_DIR/ynPrompt.sh"

result=$($YNPROMPT "Is this WSL? (y/n) ")
if [[ $result = "yes" ]]; then
  sudo echo -e "[interop]\nappendWindowsPath = false" >> /etc/wsl.conf 
  echo 'sudo mount --make-rshared /' >> $HOME/.bash_profile
  sudo chmod 4755 /usr/bin/newgidmap
  sudo chmod 4755 /usr/bin/newuidmap
fi
