#!/bin/bash

AppName=${PWD##*/}-py3

podman run -it \
  --rm \
  --network=host \
  -v .:/opt/$AppName:z \
  -w /opt/$AppName \
  --name $AppName \
  python:3 bash
