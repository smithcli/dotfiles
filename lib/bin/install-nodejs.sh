#!/usr/bin/env bash

#Script to install Nodejs Runtime using pnpm

install() {
  echo "Installing Pnpm"
  mkdir -p $PNPM_HOME
  curl -fsSL https://get.pnpm.io/install.sh | sh -
  PATH="$PNPM_HOME:$PATH"
}

setUp() {
  echo "Installing nodejs lts"
  pnpm env use --global lts
}

cleanUp() {
  if [[ $CLEAN_UP ]]; then
    echo "Cleaning up bashrc"  
    head -n -8 $HOME/.bashrc > tmp && mv tmp $HOME/.dotfiles/bash/.bashrc
  fi
}

if [[ -z "$PNPM_HOME" ]]; then
  export PNPM_HOME="$HOME/.local/lib/nodejs/pnpm"
else
  CLEAN_UP=true
fi

install
setUp
cleanUp

echo "Pnpm installed at $PNPM_HOME"
