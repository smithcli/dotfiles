#!/bin/bash

AppName=${PWD##*/}-aws-cli

podman run -it \
  --rm \
  --network=host \
  -v .:/opt/$AppName:z \
  -w /opt/$AppName \
  --name $AppName \
  --entrypoint bash \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   public.ecr.aws/aws-cli/aws-cli
