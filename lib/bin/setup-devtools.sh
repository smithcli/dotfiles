#!/usr/bin/env bash
echo -e "\n=>> Running lsp setup\n"
source $HOME/.dotfiles/lib/components/download_release.sh

# Distrobox preparation
if [[ -n $DISTROBOX_HOST_HOME ]]; then
  HOME=$DISTROBOX_HOST_HOME

  PATH="$DISTROBOX_HOST_HOME/.local/bin:$PATH"

  export CARGO_HOME="$HOME/.local/lib/rust/cargo"
  export RUSTUP_HOME="$HOME/.local/lib/rust/rustup"
  if [ -f "$HOME/.local/lib/rust/cargo/env" ]; then
    source "$HOME/.local/lib/rust/cargo/env"
  fi

  export PNPM_HOME="$HOME/.local/lib/nodejs/pnpm"
  if [ -d "$PNPM_HOME" ]; then
    PATH="$PNPM_HOME:$PATH"
  fi
fi

add_rustup_lsp() {
  if ! rustup_loc="$(type -p "rustup")" || [[ -z $rustup_loc ]]; then
    echo "rustup is missing"
    exit 1
  fi
  ## Rust
  rustup component add rust-analyzer # requires gcc
}

add_cargo_lsp() {
  if ! cargo_loc="$(type -p "cargo")" || [[ -z $cargo_loc ]]; then
    echo "cargo is missing"
    exit 1
  fi
  ## TOML
  cargo install taplo-cli --locked --features lsp
  ## Delta - git-diff
  cargo install git-delta
}

add_node_lsp() {
  local pkg_mgr
  # check installed
  if pnpm_loc="$(type -p "pnpm")" || [[ -n $pnpm_loc ]]; then
    pkg_mgr=pnpm
  elif npm_loc="$(type -p "npm")" || [[ -n $npm_loc ]]; then
    pkg_mgr=pnpm
  else
    echo "npm is missing"
    exit 1
  fi

  ## Ansible
  $pkg_mgr install -g @ansible/ansible-language-server
  ## Astro
  $pkg_mgr install -g @astrojs/language-server @astrojs/ts-plugin prettier-plugin-astro
  ## Bash
  $pkg_mgr install -g bash-language-server
  ## Docker
  $pkg_mgr install -g dockerfile-language-server-nodejs
  ## Formatter
  $pkg_mgr install -g prettier
  ## Typescript
  $pkg_mgr install -g typescript typescript-language-server
  ## HTML, CSS, JSON, SCSS
  $pkg_mgr install -g vscode-langservers-extracted
  ## SQL
  $pkg_mgr install -g sql-language-server
  ## Svelte
  $pkg_mgr install -g svelte-language-server typescript-svelte-plugin
  ## Vue
  $pkg_mgr install -g @vue/language-server
  ## YAML
  $pkg_mgr install -g yaml-language-server@next
}

add_python_lsp() {
  if ! pip3_loc="$(type -p "pip3")" || [[ -z $pip3_loc ]]; then
    echo "pip3 is missing"
    exit 1
  fi
  pip3 install -U \
    black \
    'python-lsp-server[all]' \
    pylsp-rope
}

_get_release() {
  local base_url=$1
  local release_name=$2
  local bin_name=$3

  if ! binary_loc="$(type -p "$bin_name")" || [[ -z $binary_loc ]]; then
    download_release $base_url $release_name $bin_name

    release_name=${release_name/"[version]"/*}
    echo $release_name
    chmod u+x $release_name
    mv $release_name* $HOME/.local/opt
    ln -srnf $HOME/.local/opt/$release_name $HOME/.local/bin/$bin_name
  fi
}

add_download_lsp() {
  # curl installed
  if ! curl_loc="$(type -p "curl")" || [[ -z $curl_loc ]]; then
    echo "curl is missing"
    exit 1
  fi

  ## Lua
  # _get_release \
  #   https://github.com/LuaLS/lua-language-server/releases \
  #   lua-language-server-[version]-linux-x64 \
  #   lua-language-server

  ## Markdown
  _get_release \
    https://github.com/artempyanykh/marksman/releases \
    marksman-linux-x64 \
    marksman

  ## Shellcheck
  _get_release \
    https://github.com/koalaman/shellcheck/releases/ \
    shellcheck-[version].linux.x86_64 \
    shellcheck

  ## Shfmt
  _get_release \
    https://github.com/mvdan/sh/releases/ \
    shfmt_[version]_linux_amd64 \
    shfmt

  ## XML
  # Doesn't work
  # _get_release \
  #   https://github.com/redhat-developer/vscode-xml/releases \
  #   lemminx-linux.zip \
  #   lemminx
}

add_rustup_lsp
add_cargo_lsp
add_node_lsp
add_download_lsp
