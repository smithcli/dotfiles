#!/usr/bin/env bash

# Creates a backup archive of gpg and ssh keys
# For more info see https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration.html

# NOTE: To delete primary secret key after backup: 
# gpg --delete-secret-keys john.doe@example.com

BACKUP_DIR="$HOME/.backup"
GPG_BACKUP_DIR="gpg"
SSH_BACKUP_DIR="ssh"
DATE=$(date +%Y-%m-%d)
BACKUP_NAME="$DATE-key-backup.tar.xz"
EXPORT_NAME="$DATE-key-export.tar.xz"
KEY_EXPORT=false

log_error() { echo -e "\e[31m$*\e[0m"; }
log_info() { echo -e "\e[32m$*\e[0m"; }

# Print help information
print_help() {
  echo "Creates a backup archive of GnuPG keys and SSH at $BACKUP_DIR."
  echo "You can restore this back up using the key-restore script."
  echo "Usage: $0 [OPTIONS]"
  echo "Options:"
  echo "  -x, --export       Creates a backup but with gpg subkeys only."
  echo "                     This option is to make it portable and help"
  echo "                     protect the primary secret key."
  echo "  -h, --help         Display this help message and exit."
}

# Parse command-line arguments
while [[ "$1" != "" ]]; do
  case $1 in
  -x | --export)
    KEY_EXPORT=true
    ;;
  --help)
    print_help
    exit 0
    ;;
  *)
    echo "Unknown option: $1"
    print_help
    exit 1
    ;;
  esac
  shift
done

# Backup GPG
_backup_gpg() {
  mkdir -vp $GPG_BACKUP_DIR

  gpg --armor --export >$GPG_BACKUP_DIR/pub.asc
  local export_option="--export-secret-keys"
  [[ $KEY_EXPORT == true ]] && export_option="--export-secret--subkeys"
  gpg --armor $export_option >$GPG_BACKUP_DIR/priv.asc
  gpg --export-ownertrust >$GPG_BACKUP_DIR/trust.txt

  # GPG configuration file
  if [ -f $HOME/.gnupg/gpg.conf ]; then
    cp -v $HOME/.gnupg/gpg.conf $GPG_BACKUP_DIR
  fi

  # GPG configuration file
  if [ -f $HOME/.gnupg/common.conf ]; then
    cp -v $HOME/.gnupg/common.conf $GPG_BACKUP_DIR
  fi

  # GPG-agent configuration
  if [ -f $HOME/.gnupg/gpg-agent.conf ]; then
    cp -v $HOME/.gnupg/gpg-agent.conf $GPG_BACKUP_DIR
  fi

  # GPG 2 SSH configuration
  if [ -f $HOME/.gnupg/sshcontrol ]; then
    cp -v $HOME/.gnupg/sshcontrol $GPG_BACKUP_DIR
  fi

  # git authentication token
  if [ -f $HOME/.gnupg/gitoken.gpg ]; then
    cp -v $HOME/.gnupg/gitoken.gpg $GPG_BACKUP_DIR
  fi

  # Pre-generate revocation certificates (if you lose your primary secret key)
  if [ -d $HOME/.gnupg/openpgp-revocs.d ]; then
    cp -v --recursive $HOME/.gnupg/openpgp-revocs.d $GPG_BACKUP_DIR
  fi

  log_info "GnuPG keys and configuration backed up."
}

# Backup SSH
_backup_ssh() {
  cp -v --recursive $HOME/.ssh $SSH_BACKUP_DIR
  log_info "SSH keys and configuration backed up."
}

# Remove files not in archive
_cleanup_files() {
  # write over sensitive
  shred -u \
    $GPG_BACKUP_DIR/pub.asc \
    $GPG_BACKUP_DIR/priv.asc \
    $GPG_BACKUP_DIR/trust.txt \
    $GPG_BACKUP_DIR/gpg.conf \
    $GPG_BACKUP_DIR/gitoken.gpg \
    $GPG_BACKUP_DIR/openpgp-revocs.d/* \
    $SSH_BACKUP_DIR/*
  # remove directories
  rm -r $GPG_BACKUP_DIR
  rm -r $SSH_BACKUP_DIR
}

create_backup() {
  log_info "Creating key backup"
  mkdir -p $BACKUP_DIR
  chmod 700 $BACKUP_DIR
  cd $BACKUP_DIR
  if [ -d $HOME/.gnupg ]; then _backup_gpg; fi
  if [ -d $HOME/.ssh ]; then _backup_ssh; fi
  # create tar archive
  local archive_name="$BACKUP_NAME"
  [[ $KEY_EXPORT == true ]] && archive_name="$EXPORT_NAME"
  tar -I 'xz -9' -cf $archive_name $GPG_BACKUP_DIR $SSH_BACKUP_DIR
  chmod 600 $archive_name
  log_info "Backup created at $BACKUP_DIR/$archive_name"
  _cleanup_files
}

create_backup
