#!/usr/bin/env bash

podman run \
  -ti \
  --rm \
  --name mkpasswd \
  quay.io/coreos/mkpasswd \
  --stdin \
  --method=yescrypt \
  $1
