#!/usr/bin/env bash

podman run \
  --rm \
  --name butane \
  --interactive \
  --security-opt label=disable \
  --volume "${PWD}":/pwd \
  --workdir /pwd \
  quay.io/coreos/butane:release \
  "${@}"
