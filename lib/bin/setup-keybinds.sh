#!/usr/bin/env bash
source $HOME/.dotfiles/lib/components/logger.sh
log_info "Running keybind setup"

if [[ "$XDG_CURRENT_DESKTOP" == "GNOME" ]]; then
  log_debug "Setting keyinds in Gnome"
  gsettings set org.gnome.desktop.wm.keybindings minimize "['<Super>z']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-1 "['<Shift><Alt><Super>1']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-2 "['<Shift><Alt><Super>2']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-3 "['<Shift><Alt><Super>3']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-4 "['<Shift><Alt><Super>4']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left "['<Shift><Alt><Super>bracketleft']"
  gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "['<Shift><Alt><Super>bracketright']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Alt><Super>1']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Alt><Super>2']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Alt><Super>3']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Alt><Super>4']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['<Alt><Super>bracketleft']"
  gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Alt><Super>bracketright']"
  gsettings set org.gnome.mutter.wayland.keybindings restore-shortcuts "['<Shift><Control><Super>Escape']"
  gsettings set org.gnome.settings-daemon.plugins.media-keys screensaver "['<Super>Escape']"
fi
