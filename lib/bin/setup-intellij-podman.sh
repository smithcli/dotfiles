#!/bin/bash

# To use podman in Intellij, must start API service
# https://www.jetbrains.com/help/idea/podman.html#tutorial-run-podman-in-a-virtual-machine
# unix:///var/run/user/1000/podman/podman.sock
podman system service -t 0
