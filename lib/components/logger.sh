#!/usr/bin/env bash

# Logger Utility Script

RESET="\\033[0m"
RED="\\033[91m"
YELLOW="\\033[93m"
GREEN="\\033[92m"
BLUE="\\033[94m"

# LOG_FILE="/var/log/logger.log"
DATE=$(date +"%Y-%m-%d %H:%M:%S")

log() {
  local MESSAGE=$1
  local LEVEL=$2
  local TIMESTAMP=$3

  if [ -z "$TIMESTAMP" ]; then
    TIMESTAMP=$DATE
  fi

  # printf "$(timestamp_to_log_format $TIMESTAMP) [$LEVEL] $MESSAGE"
  case $LEVEL in
  ERROR)
    printf "${RED}[$LEVEL]${RESET} $MESSAGE\n"
    # printf "$(timestamp_to_log_format $TIMESTAMP) [$LEVEL] $MESSAGE\n >> $LOG_FILE"
    ;;
  WARNING)
    printf "${YELLOW}[$LEVEL]${RESET} $MESSAGE\n"
    # printf "$(timestamp_to_log_format $TIMESTAMP) [$LEVEL] $MESSAGE\n >> $LOG_FILE"
    ;;
  INFO)
    printf "${GREEN}[$LEVEL]${RESET} $MESSAGE\n"
    # printf "$(timestamp_to_log_format $TIMESTAMP) [$LEVEL] $MESSAGE\n >> $LOG_FILE"
    ;;
  DEBUG)
    printf "${BLUE}[$LEVEL]${RESET} $MESSAGE\n"
    # printf "$(timestamp_to_log_format $TIMESTAMP) [$LEVEL] $MESSAGE\n >> $LOG_FILE"
    ;;
  *)
    printf "$MESSAGE\n"
    # printf "$(timestamp_to_log_format $TIMESTAMP) $MESSAGE\n >> $LOG_FILE"
    ;;
  esac
}

timestamp_to_log_format() {
  # Converts timestamp to YYYY-MM-DD HH:MM:SS format
  local TIMESTAMP=$1
  date +"%Y-%m-%d %H:%M:%S"
}

log_error() {
  log "$@" "ERROR"
}

log_warning() {
  log "$@" "WARNING"
}

log_info() {
  log "$@" "INFO"
}

log_debug() {
  log "$@" "DEBUG"
}
