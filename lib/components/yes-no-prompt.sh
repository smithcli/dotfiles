#!/bin/bash

# loop that forces a yes or no answer
# allows arguement to place in prompt

while true
do
  read -r -p "$1" input
  case $input in
    [yY][eE][sS]|[yY])
      echo "yes"
      break;;
    [nN][oO]|[nN])
      echo "no"
      break;;
    *)
      echo "Invalid input ";;
  esac      
done
