# Downloads helix editor and adds to local/bin
source $HOME/.dotfiles/lib/components/get_release.sh

download_helix() {
  echo "Downloading helix"
  local arch=$(uname -m)

  get_release \
    https://github.com/helix-editor/helix/releases \
    helix-[version]-$arch-linux \
    hx
}
