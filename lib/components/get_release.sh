# Downloads release to local/opt and places binary in local/bin
source $HOME/.dotfiles/lib/components/download_release.sh

get_release() {
  local url=$1
  local release_name=$2
  local bin_name=$3

  # Checks to see if binary already exists in PATH
  # If it does NOT then it performs the download
  if ! binary_loc="$(type -p "$bin_name")" || [[ -z $binary_loc ]]; then
    download_release $url $release_name $bin_name

    release_name=${release_name/"[version]"/*} # change variable to match bin
    echo $release_name
    chmod u+x $release_name
    mkdir -p $HOME/.local/opt $HOME/.local/bin
    mv $release_name* $HOME/.local/opt
    ln -srnf $HOME/.local/opt/$release_name $HOME/.local/bin/$bin_name
  fi
}
