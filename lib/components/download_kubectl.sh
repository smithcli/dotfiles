# Downloads kubectl and adds to local/bin
source $HOME/.dotfiles/lib/components/get_release.sh

download_kubectl() {
  echo "Downloading kubectl"
  local arch=$(uname -m)
  [ "$arch" == "x86_64" ] && arch="amd64"
  [ "$arch" == "aarch64" ] && arch="arm64"

  local version=$(curl -L -s https://dl.k8s.io/release/stable.txt)

  get_release \
    "https://dl.k8s.io/release/${version}/bin/linux/${arch}/kubectl" \
    "kubectl_${version}_linux_${arch}" \
    kubectl

  # checksum
  # curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
}
