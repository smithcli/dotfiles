# Downloads helix editor and adds to local/bin
source $HOME/.dotfiles/lib/components/get_release.sh

download_shfmt() {
  echo "Downloading shfmt"
  local arch=$(uname -m)
  [ "$arch" == "x86_64" ] && arch="amd64"
  [ "$arch" == "aarch64" ] && arch="arm64"

  get_release \
    https://github.com/mvdan/sh/releases/ \
    shfmt_[version]_linux_$arch \
    shfmt
}
