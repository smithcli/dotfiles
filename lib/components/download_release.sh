# components to aid package management

# example:

# download_release \
#   https://github.com/LuaLS/lua-language-server/releases \
#   lua-language-server-[version]-linux-x64 \
#   lua-language-server

# TODO: checksum - validate download

download_release() {
  local url=$1
  local release_name=$2
  local bin_name=$3
  # TODO: check if release exists
  # TODO: check if url is for github?
  # TODO: check if latest version
  # Check if the URL contains github
  if [[ $url == https://github.com/* ]]; then
    local version=$(curl --silent -qI $url/latest | awk -F '/' '/^location/ {print  substr($NF, 1, length($NF)-1)}')
    release_name=${release_name/"[version]"/$version}
    curl -LJO "${url}/download/${version}/${release_name}"
  else
    curl -o $release_name -LJ $url
  fi
}

# Explanation of the arguments
# -L, --location Follow redirects
# -J, --remote-header-name Use the header-provided filename
# -O, --remote-name Write output to a file named as the remote file

# Other ways to get releases?
# VER=$(curl --silent -qI https://github.com/bakito/adguardhome-sync/releases/latest | awk -F '/' '/^location/ {print  substr($NF, 1, length($NF)-1)}'); \
# wget https://github.com/bakito/adguardhome-sync/releases/download/$VER/adguardhome-sync_${VER#v}_linux_x86_64.tar.gz
# add uname -m for diff arch
