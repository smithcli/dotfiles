# Component to add helix to package manager
add_helix() {
  echo -e "\n==>> Adding helix repo\n"
  if [[ -n $(command -v dnf) ]]; then
    _add_dnf_helix_repo
  fi
}

_add_dnf_helix_repo() {
  echo "using dnf"
  if [[ -n $DISTROBOX_HOST_HOME ]]; then
    source $DISTROBOX_HOST_HOME/.local/bin/copr enable varlad/helix
  else
    source $HOME/.local/bin/copr enable varlad/helix
  fi
}
