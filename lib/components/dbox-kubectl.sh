# Component to add kubectl to package manager
add_kubectl() {
  echo -e "\n==>> Adding kubectl repo\n"
  local version=$(curl --location --silent https://dl.k8s.io/release/stable.txt)
  version=$(echo ${version%.*})
  if [[ -n $(command -v dnf) ]]; then
    _add_dnf_kubectl_repo $version
  fi
}

_add_dnf_kubectl_repo() {
  echo "using dnf"
  local version=$1
  # This overwrites any existing configuration in /etc/yum.repos.d/kubernetes.repo
  cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/$version/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/$version/rpm/repodata/repomd.xml.key
EOF
}
