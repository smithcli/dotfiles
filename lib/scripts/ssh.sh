generate_ssh_key() {
  local name="$1"
  local date=$(date +'%Y-%m-%d') # Current date in YYYY-MM-DD format
  local key_name="${date}-${name// /-}-ssh"
  echo "generating SSH key ${key_name}"
  ssh-keygen -t ed25519 -C "${key_name}" -f "$HOME/.ssh/${key_name}"
}
