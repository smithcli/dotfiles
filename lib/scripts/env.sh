# Exports variables from a file

# Example usage:
# source-env path/to/your/.env

source-env() {
  # Check if an argument is provided
  if [ $# -eq 0 ]; then
    echo "Usage: source-env <path_to_env_file>"
    return 1
  fi
  local env_file=$1
  # Check if the specified .env file exists
  if [ ! -f "$env_file" ]; then
    echo "Error: $env_file not found."
    return 1
  fi
  # Read the .env file line by line
  while IFS='=' read -r line || [[ -n "$line" ]]; do
    # Skip lines that start with # (comments) or lines that match certain patterns
    if [[ "$line" =~ ^\ *# || "$line" =~ ^\ *$ || "$line" =~ ^\ *export\ [a-zA-Z_]+= ]]; then
      continue
    fi
    ENVAR=$(printf %s\\n "${line%%#*}" | xargs)
    [[ -n $ENVAR ]] && export "$ENVAR"
  done <"$env_file"

  echo "Environment variables loaded from $env_file."
}

envreplace() {
  if [ $# -eq 0 ] || [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
    echo "Replaces environment variables in a file based on a defined key=value .env file."
    echo "Usage: envreplace <env_file> <replace_file>"
    echo ""
    echo "Options:"
    echo "-h, --help        Show this help message and exit"
    echo ""
    return 1
  fi
  local env_file=$1
  local input_file=$2
  local variables=""

  # Check if the specified .env file exists
  if [ ! -f "$env_file" ]; then
    echo "Error: $env_file not found."
    return 1
  fi
  # Read the .env file line by line
  while IFS='=' read -r line || [[ -n "$line" ]]; do
    # Skip lines that start with # (comments) or lines that match certain patterns
    if [[ "$line" =~ ^\ *# || "$line" =~ ^\ *$ || "$line" =~ ^\ *export\ [a-zA-Z_]+= ]]; then
      continue
    fi
    ENVAR=$(printf %s\\n "${line%%#*}" | xargs)
    [[ -n $ENVAR ]] && export "$ENVAR"
    # Extract key and value from the line
    key=$(echo "$line" | cut -d'=' -f1)
    # local value=$(echo "$line" | cut -d'=' -f2-)
    variables+="\$${key} "
  done <"$env_file"
  envsubst "'${variables% }'" <$input_file
}
