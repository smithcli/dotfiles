# For Gnome to import a wireguard config file

vpn-import-wg-config() {
  nmcli connection import type wireguard file $@
}
