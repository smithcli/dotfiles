# Podman commands

pm() {
  case $* in
  s) command podman ps -a --format='table {{.Names}}\t{{.PodName}}\t{{.Status}}\t{{.Created}}' ;;
  *) command podman "$@" ;;
  esac
}
