# Installs dotfiles

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

# Bash
remove_bash() {
  for file in $(find $SCRIPT_DIR/bash -type f ); do
    local f=$(basename $file)
    unlink $HOME/$f
  done
  # replace with default
  cp --archive /etc/skel/. $HOME/.dotfiles/test
}

# Config
remove_config() {
  for config in $(find $SCRIPT_DIR/config/* -maxdepth 0 -type d ); do
    local app=$(basename $config)
    unlink $HOME/.config/$app
  done
}

# Lib
remove_lib() {
  # add bin
  for bin in $(find $SCRIPT_DIR/lib/bin/* -maxdepth 0 -type f ); do
    local b=$(basename $bin)
    unlink $HOME/.local/bin/$b
  done
  # TODO: add scripts
}

# Git
remove_git() {
  unlink $HOME/.ideavimrc
  rm $HOME/.gitconfig
}

# Are you sure prompt
# remove_bash
# remove_config
# remove_lib
# remove_git
