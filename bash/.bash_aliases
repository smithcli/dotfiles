# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# change directory
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# XDG
alias open='xdg-open'

# Distrobox
alias db="distrobox"

# Podman
alias pm="podman"

# Pnpm
alias pn="pnpm"

# Python
alias py="python3"
alias pip="pip3"

# Neovim
alias v="nvim"

# wezterm
alias catimg="wezterm imgcat"

# fd-find for Debian distros
#alias fd="fdfind"
