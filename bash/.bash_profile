# .bash_profile
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

# Get the custom functions
if [ -d "$HOME/.dotfiles/lib/scripts" ]; then
  for script in $HOME/.dotfiles/lib/scripts/*.sh; do
    if [ -f "$script" ]; then
      . "$script"
    fi
  done
fi

# User specific environment and startup programs
if [ -d "$HOME/.local/bin" ]; then
  PATH=$PATH:"$HOME/.local/bin"
fi

# For machine specific scripts
if [ -f /etc/os-release ]; then
  # freedesktop.org and systemd
  . /etc/os-release
  export THIS_OS_MACHINE=$ID
  export THIS_VER_MACHINE=$VERSION_ID
else
  export THIS_OS_MACHINE=${uname-s}
  export THIS_VER_MACHINE=${uname-r}
fi

# GnuPG setup
if [ -d "$HOME/.gnupg" ]; then
  # SSH to use gpg-agent instead
  unset SSH_AGENT_PID
  if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  fi
  # Set GPG TTY as stated in 'man gpg-agent'
  export GPG_TTY=$(tty)
  # Refresh gpg-agent tty in case user switches into an X session
  gpg-connect-agent updatestartuptty /bye >/dev/null
  # For more details, see https://wiki.archlinux.org/index.php/GnuPG#SSH_agent
fi

export DATE=$(date +%Y-%m-%d)

# EDITOR
export VISUAL="hx"
export EDITOR="$VISUAL"

# Android
ANDROID_HOME="$HOME/.local/lib/android/sdk"
[ -d "$ANDROID_HOME/cmdline-tools/latest/bin" ] && PATH=$PATH:"$ANDROID_HOME/cmdline-tools/latest/bin"
[ -d "$ANDROID_HOME/emulator" ] && PATH=$PATH:"$ANDROID_HOME/emulator"
[ -d "$ANDROID_HOME/platform-tools" ] && PATH=$PATH:"$ANDROID_HOME/platform-tools"

# AWS
export SAM_CLI_TELEMETRY=0

# batcat or bat
export BAT_CONFIG_PATH="$HOME/.config/bat/bat.conf"

# Flutter
if [ -d "$HOME/.local/lib/flutter/bin" ]; then
  PATH=$PATH:"$HOME/.local/lib/flutter/bin"
fi

# Podman
export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock

# Pulumi
export PULUMI_INSTALL_ROOT="$HOME/.local/lib/pulumi"
export PATH=$PATH:"$HOME/.local/lib/pulumi/bin"

# Python
if [ -d "$HOME/.local/lib/python/bin" ]; then
  PATH=$PATH:"$HOME/.local/lib/python/bin"
fi
export PYTHONUSERBASE="$HOME/.local/lib/python"

# Rust
export CARGO_HOME="$HOME/.local/lib/rust/cargo"
export RUSTUP_HOME="$HOME/.local/lib/rust/rustup"
if [ -f "$HOME/.local/lib/rust/cargo/env" ]; then
  . "$HOME/.local/lib/rust/cargo/env"
fi

# Nodejs
export PNPM_HOME="$HOME/.local/lib/nodejs/pnpm"
if [ -d "$PNPM_HOME" ]; then
  PATH=$PATH:"$PNPM_HOME"
fi

# K3S
export KUBECONFIG="$HOME/.kube/config.yml"
export INSTALL_K3S_BIN_DIR="$HOME/.local/bin"
export KUBE_EDITOR="$EDITOR"

# Deno
# export DENO_INSTALL="$HOME/.local/lib/deno"
# export PATH="$DENO_INSTALL/bin:$PATH"
