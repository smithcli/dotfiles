# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
  debian_chroot=$(cat /etc/debian_chroot)
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# FZF
if [ -f /usr/share/fzf/shell/key-bindings.bash ]; then
  . /usr/share/fzf/shell/key-bindings.bash
fi
export FZF_DEFAULT_COMMAND='fd --type f --color=never --hidden --ignore-file ~/.config/fd/ignore'
export FZF_DEFAULT_OPTS='--reverse --no-height'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'bat --color=always --line-range :50 {}'"
export FZF_ALT_C_COMMAND='fd --type d . --color=never --hidden'
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -50'"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
    . <(kubectl completion bash)
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Alias completions.
[ -f ~/.config/shell/alias_completions.sh ] && . ~/.config/shell/alias_completions.sh
# Git
[ -f ~/.config/shell/git-prompt.sh ] && . ~/.config/shell/git-prompt.sh
# Flutter completions.
[ -f ~/.config/shell/flutter_completions.sh ] && . ~/.config/shell/flutter_completions.sh
# pnpm tabtab source for packages, uninstall by removing these lines
[ -f ~/.config/tabtab/bash/__tabtab.bash ] && . ~/.config/tabtab/bash/__tabtab.bash || true

# Bash Prompt
configure_prompt() {
  local hn=$([ -z "$CONTAINER_ID" ] && echo "\h" || echo "$CONTAINER_ID")
  local prompt_symbol=㉿
  local green_light="\[\e[38;5;70m\]"
  local green_dark="\[\e[38;5;28m\]"
  local blue="\[\e[38;5;33m\]"
  local orange="\[\e[38;5;208m\]"
  local reset="\[\033[0m\]"
  local pointer=$'\u2771'
  # PS1="\[\e[38;5;70m\]\u\[\e[38;5;28m\]$prompt_symbol\[\e[38;5;28m\]\h \[\e[38;5;33m\]\w \[\033[0m\]$ "
  PS1="${green_light}┌──\u${green_dark}${prompt_symbol}${hn}${orange}\${PS1_CMD1} ${blue}\w\n${green_light}└─ ${orange}${pointer} ${reset}"
  # PS1="\[\e[38;5;70m\]┌──\u\[\e[38;5;28m\]$prompt_symbol\[\e[38;5;28m\]\h \[\e[38;5;33m\]\w \n\[\e[38;5;70m\]└─\[\033[0m\] $orange_pointer "
  # unset prompt_symbol green_light green_dark blue orange pointer no_color
}
PROMPT_COMMAND='PS1_CMD1=$(__git_ps1 " (%s)")'
configure_prompt
