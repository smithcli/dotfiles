#!/usr/bin/env bash

source $HOME/.dotfiles/lib/components/logger.sh
log_info "Running distrobox-init"

# For using alternate home dir
install_profile() {
  log_info "Installing profile"
  if [[ -n "$DISTROBOX_HOST_HOME" ]]; then
    log_info "Using Alternate Home"
    install -vm 644 -t $HOME $DISTROBOX_HOST_HOME/.bashrc
    install -vm 644 -t $HOME $DISTROBOX_HOST_HOME/.bash_aliases
    install -vm 644 -t $HOME $DISTROBOX_HOST_HOME/.bash_logout
    install -vm 644 -t $HOME $DISTROBOX_HOST_HOME/.gitconfig
    ln -srnf $DISTROBOX_HOST_HOME/.config $HOME/.config
    ln -srnf $DISTROBOX_HOST_HOME/.gnupg $HOME/.gnupg
    ln -srnf $DISTROBOX_HOST_HOME/.ssh $HOME/.ssh
    mkdir -p $DISTROBOX_HOST_HOME/.local/bin
    mkdir -p $DISTROBOX_HOST_HOME/.local/lib
    mkdir -p $DISTROBOX_HOST_HOME/.local/opt
    mkdir -p $DISTROBOX_HOST_HOME/.local/share
    chown $USER:$USER $DISTROBOX_HOST_HOME/.bashrc
    chown $USER:$USER $DISTROBOX_HOST_HOME/.bash_aliases
    chown $USER:$USER $DISTROBOX_HOST_HOME/.bash_logout
    chown $USER:$USER $DISTROBOX_HOST_HOME/.gitconfig
    chown $USER:$USER $DISTROBOX_HOST_HOME/.local
    chown $USER:$USER $DISTROBOX_HOST_HOME/.cache
  fi
  log_info "Using User Home"
}

install_base_packages() {
  log_info "Installing base packages"
  local home=""
  [[ -n "$DISTROBOX_HOST_HOME" ]] && home="$DISTROBOX_HOST_HOME" || home="$HOME"
  log_debug "Using $home"
  source $home/.dotfiles/lib/components/dbox-helix.sh
  source $home/.dotfiles/lib/components/dbox-kubectl.sh

  add_helix
  add_kubectl

  dnf install -y \
    git \
    gcc \
    make \
    fd-find \
    bat \
    fzf \
    pinentry \
    flatpak-builder \
    openssl \
    wl-clipboard \
    kubectl \
    helix

  bat cache --build
}

install_profile
install_base_packages
