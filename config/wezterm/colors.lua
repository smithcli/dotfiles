local green_light = 'hsl(94.89 41% 31%)'
local green_lightb = 'hsl(93.67 53% 25%)'
local green_dark = 'hsl(104.8 52% 19%)'
local blue = 'hsl(216.67, 0%, 75.29%)'
local orange_active = 'hsl(30.41 62% 52%)'
local orange_inactive = 'hsl(33.7 91% 36% / .6 )'

return {
  selection_fg = 'Black',
  selection_bg = orange_active,
  -- cursor_fg = 'white',
  -- cursor_bg = green_light,
  split = green_light,
  copy_mode_active_highlight_bg = { Color = orange_active },
  copy_mode_active_highlight_fg = { Color = 'Black' },
  copy_mode_inactive_highlight_bg = { Color = orange_inactive },
  copy_mode_inactive_highlight_fg = { Color = 'Black' },

  quick_select_label_bg = { Color = 'cyan' },
  quick_select_label_fg = { Color = 'Black' },
  quick_select_match_bg = { Color = 'blue' },
  quick_select_match_fg = { Color = '#ffffff' },
  tab_bar = {
    background = "#1e1e1e",
    active_tab = {
      bg_color = orange_active,
      fg_color = 'Black',
    },
    inactive_tab = {
      bg_color = '#1e1e1e',
      fg_color = 'white',
    },
    new_tab = {
      bg_color = '#1e1e1e',
      fg_color = 'white',
    },
  }
}

-- ("Dark+", "[colors]\nansi = [\n    \"#000000\",\n    \"#cd3131\",\n    \"#0dbc79\",\n    \"#e5e510\",\n    \"#2472c8\",\n    \"#bc3fbc\",\n    \"#11a8cd\",\n    \"#e5e5e5\",\n]\nbackground = \"#1e1e1e\"\nbrights = [\n    \"#666666\",\n    \"#f14c4c\",\n    \"#23d18b\",\n    \"#f5f543\",\n    \"#3b8eea\",\n    \"#d670d6\",\n    \"#29b8db\",\n    \"#e5e5e5\",\n]\ncursor_bg = \"#ffffff\"\ncursor_border = \"#ffffff\"\ncursor_fg = \"#000000\"\nforeground = \"#cccccc\"\nselection_bg = \"#3a3d41\"\nselection_fg = \"#e0e0e0\"\n\n[colors.indexed]\n\n[metadata]\naliases = []\nname = \"Dark+\"\norigin_url = \"https://github.com/mbadolato/iTerm2-Color-Schemes\"\nwezterm_version = \"Always\"\n"),
