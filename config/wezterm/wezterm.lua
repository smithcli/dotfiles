local wezterm = require 'wezterm'
local keys = require 'keys'
local colors = require 'colors'

local config = {}
config.audible_bell = "Disabled"
config.color_scheme = 'Dark+'
config.check_for_updates_interval_seconds = 604800
config.hide_tab_bar_if_only_one_tab = true;
config.tab_bar_at_bottom = true;
config.use_fancy_tab_bar = false
config.window_decorations = "RESIZE"
config.leader = keys.leader
config.keys = keys.keys
config.colors = colors
config.font = wezterm.font_with_fallback {
  'DejaVuSansM Nerd Font',
  'JetBrains Mono',
}

if wezterm.target_triple == 'x86_64-pc-windows-msvc' then
  -- x86_64-pc-windows-msvc - Windows
  -- x86_64-apple-darwin - macOS (Intel)
  -- aarch64-apple-darwin - macOS (Apple Silicon)
  -- x86_64-unknown-linux-gnu - Linux
end

-- Change title
-- TODO: change title to 1 git project or 2 directory
-- wezterm.on('format-tab-title', function(tab)
--   local pane = tab.active_pane
--   -- local title = pane.title
--   local title = pane.get_current_working_dir()
--   wezterm.log_info(title)
--   title = title:gsub(".*:", '')
--   title = title:gsub(".*/", '')
--   return title
-- end)

return config
