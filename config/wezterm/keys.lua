local wezterm = require 'wezterm'
local act = wezterm.action

local keybinds = {}
keybinds.leader = { key = 'Space', mods = 'CTRL', timeout_milliseconds = 1000 }
keybinds.keys = {
  { key = 'm', mods = 'SUPER',     action = act.DisableDefaultAssignment, },
  { key = 'h', mods = 'SUPER',     action = act.DisableDefaultAssignment, },
  -- Activate Panes
  { key = 'h', mods = 'ALT',       action = act.ActivatePaneDirection 'Left', },
  { key = 'j', mods = 'ALT',       action = act.ActivatePaneDirection 'Down', },
  { key = 'k', mods = 'ALT',       action = act.ActivatePaneDirection 'Up', },
  { key = 'l', mods = 'ALT',       action = act.ActivatePaneDirection 'Right', },
  { key = '1', mods = 'ALT',       action = act.ActivateTab(0), },
  { key = '2', mods = 'ALT',       action = act.ActivateTab(1), },
  { key = '3', mods = 'ALT',       action = act.ActivateTab(2), },
  { key = '4', mods = 'ALT',       action = act.ActivateTab(3), },
  { key = '5', mods = 'ALT',       action = act.ActivateTab(4), },
  { key = '6', mods = 'ALT',       action = act.ActivateTab(5), },
  { key = '7', mods = 'ALT',       action = act.ActivateTab(6), },
  { key = '8', mods = 'ALT',       action = act.ActivateTab(7), },
  { key = '9', mods = 'ALT',       action = act.ActivateTab(8), },
  -- Split Panes
  { key = 'h', mods = 'SHIFT|ALT', action = act.SplitPane { direction = 'Left', }, },
  { key = 'j', mods = 'SHIFT|ALT', action = act.SplitPane { direction = 'Down', }, },
  { key = 'k', mods = 'SHIFT|ALT', action = act.SplitPane { direction = 'Up', }, },
  { key = 'l', mods = 'SHIFT|ALT', action = act.SplitPane { direction = 'Right', }, },
  -- Tabs
  { key = 'w', mods = 'ALT',       action = act.CloseCurrentTab { confirm = false }, },
  { key = 't', mods = 'ALT',       action = act.SpawnTab "CurrentPaneDomain" },
  { key = 't', mods = 'SHIFT|ALT', action = act.SpawnTab 'DefaultDomain' },
  -- Scroll Page
  { key = 'u', mods = 'SHIFT|ALT', action = act.ScrollByPage(-1) },
  { key = 'd', mods = 'SHIFT|ALT', action = act.ScrollByPage(1) },
  { key = 'u', mods = 'ALT',       action = act.ScrollByPage(-0.5) },
  { key = 'd', mods = 'ALT',       action = act.ScrollByPage(0.5) },
  -- Other
  { key = 'c', mods = 'LEADER',    action = act.ClearSelection },
}
return keybinds
