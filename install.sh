# Installs dotfiles

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

build_base() {
  mkdir -p $HOME/.config
  mkdir -p $HOME/.local/bin
  mkdir -p $HOME/.local/lib
  mkdir -p $HOME/.local/opt
}

# Bash
build_bash() {
  for file in $(find $SCRIPT_DIR/bash -type f ); do
    local f=$(basename $file)
    ln -srnf $file $HOME/$f
  done
}

# Config
build_config() {
  for config in $(find $SCRIPT_DIR/config/* -maxdepth 0 -type d ); do
    local app=$(basename $config)
    ln -srnf $config $HOME/.config/$app
  done
}

# Lib
build_lib() {
  for bin in $(find $SCRIPT_DIR/lib/bin/* -maxdepth 0 -type f ); do
    local b=$(basename -s .sh $bin)
    ln -srnf $bin $HOME/.local/bin/$b
  done
}

build_external() {
  for bin in $(find $SCRIPT_DIR/external/bin/* -type f ); do
    local b=$(basename -s .sh $bin)
    ln -srnf $bin $HOME/.local/bin/$b
  done
}

# Git
build_git() {
    ln -srnf $SCRIPT_DIR/git/.ideavimrc $HOME/.ideavimrc
# - If not .env is found give interative input for env vars
# - TODO: add reads for interactive input

# check for .gitconfig -if exits skip
## cp ./.gitconfig $HOME/
## - Read from .env
## git config --global user.name "$GIT_AUTHOR_NAME"
## git config --global user.email "$GIT_AUTHOR_EMAIL"
## git config --global credential.helper "$GIT_CRED_HELPER"
}

build_base
build_bash
build_config
build_lib
build_external
build_git
